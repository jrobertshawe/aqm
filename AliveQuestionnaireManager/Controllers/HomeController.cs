﻿﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web.Mvc;

using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using AliveQuestionnaireManager.Models;

namespace AliveQuestionnaireManager.Controllers
{
    public class HomeController : Controller
    {
        private Entities db = new Entities();
         [Authorize]
        public ActionResult Index()
        {
      
            return View();
        }
     
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetQuestionnaire().ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Create([DataSourceRequest] DataSourceRequest request, QuestionnaireViewModel model)
        {
            if (ModelState.IsValid)
            {
                Questionnaire q = new Questionnaire();
                q.name = model.Name;
                q.token = Guid.NewGuid();
                db.Questionnaires.Add(q);
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Update([DataSourceRequest] DataSourceRequest request, QuestionnaireViewModel model)
        {
            if (ModelState.IsValid)
            {
                Questionnaire q = db.Questionnaires.SingleOrDefault(i => i.id == model.id);
                q.name = model.Name;
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Destroy([DataSourceRequest] DataSourceRequest request, QuestionnaireViewModel model)
        {
            Questionnaire q = db.Questionnaires.SingleOrDefault(i => i.id == model.id);
            db.Questionnaires.Remove(q);
            db.SaveChanges();

            return Json(ModelState.ToDataSourceResult());
        }
        private  IEnumerable<QuestionnaireViewModel> GetQuestionnaire()
        {

            return db.Questionnaires.Select(questionnaire => new QuestionnaireViewModel
            {
                id = questionnaire.id,
                Name = questionnaire.name
            });
        }
    }
}
