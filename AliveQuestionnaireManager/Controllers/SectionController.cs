﻿﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web.Mvc;

using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using AliveQuestionnaireManager.Models;

namespace AliveQuestionnaireManager.Controllers
{
    public class SectionController : Controller
    {
        //
        // GET: /Section/
        private Entities db = new Entities();
        public ActionResult Index(string id)
        {
            ViewBag.segmentId = id;
            return View();
        }
        public ActionResult Read(int segmentId, [DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetSection(segmentId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Create(int segmentId, [DataSourceRequest] DataSourceRequest request, SectionViewModel model)
        {
            if (ModelState.IsValid)
            {
                Section s = new Section();
                s.sectionTitle = model.sectionTitle;
                s.sectionHidden = model.sectionHidden;
                s.image = model.image;
                s.sectionArray = model.sectionArray;
                s.moveToTop = model.moveToTop;
                s.recreateSection = model.recreateSection;
                s.showTableFooter = model.showTableFooter;
                s.valueName = model.valueName;
                s.arrayName = model.arrayName;
                s.onValueChange = model.valueChangeObject.name;
                s.valueAutomatic = model.valueAutomatic;
                s.valueIsGPSLocation = model.valueIsGPSLocation;
                Segment seg = db.Segments.SingleOrDefault(i => i.id == segmentId);
                if (seg != null)
                {
                    seg.Sections.Add(s);
                }
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Destroy([DataSourceRequest] DataSourceRequest request, SectionViewModel model)
        {
            Section s = db.Sections.SingleOrDefault(i => i.id == model.id);
            db.Sections.Remove(s);
            db.SaveChanges();

            return Json(ModelState.ToDataSourceResult());
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Update([DataSourceRequest] DataSourceRequest request, SectionViewModel model)
        {
            if (ModelState.IsValid)
            {
                Section s = db.Sections.SingleOrDefault(i => i.id == model.id);
                s.sectionTitle = model.sectionTitle;
                s.sectionHidden = model.sectionHidden;
                s.image = model.image;
                s.moveToTop = model.moveToTop;
                s.sectionArray = model.sectionArray;
                s.recreateSection = model.recreateSection;
                s.showTableFooter = model.showTableFooter;
                s.valueName = model.valueName;
                s.arrayName = model.arrayName;
                s.onValueChange = model.valueChangeObject.name;
                s.valueAutomatic = model.valueAutomatic;
                s.valueIsGPSLocation = model.valueIsGPSLocation;

                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        private IEnumerable<SectionViewModel> GetSection(int segmentId)
        {
            List<Section> list = new List<Section>();
            List<SectionViewModel> models = new List<SectionViewModel>();
            list = db.Sections.Where(s => s.segmentId == segmentId).ToList();
            foreach (Section section in list)
            {
                SectionViewModel model = new SectionViewModel();
                 model.id = section.id;
                model.sectionTitle = section.sectionTitle;
                model.sectionHidden = (bool)section.sectionHidden;
                model.image = section.image;
                model.moveToTop = (bool)section.moveToTop;
                model.sectionArray = (bool)section.sectionArray;
                model.recreateSection = (bool)section.recreateSection;
                model.showTableFooter = (bool)section.showTableFooter;
                model.arrayDefaultCount = section.ArrayDefaults.Count();
                model.arrayName = section.arrayName;
                model.valueName = section.valueName;
                model.valueChangeObject.name = section.onValueChange;
                model.valueIsGPSLocation = (bool)section.valueIsGPSLocation;
                model.valueAutomatic = (bool)section.valueAutomatic;

                models.Add(model);

            }
            return models;
           
        }
    }
}
