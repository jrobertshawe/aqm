﻿﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web.Mvc;

using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using AliveQuestionnaireManager.Models;

namespace AliveQuestionnaireManager.Controllers
{
    public class SectionCellRadioController : Controller
    {
        //
        // GET: /Section/
        private Entities db = new Entities();
        public ActionResult Index(string id)
        {
            ViewBag.sectionCellId = id;
            return View();
        }
        public ActionResult Read(int sectionCellId, [DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetRadios(sectionCellId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Create(int sectionCellId, [DataSourceRequest] DataSourceRequest request, SectionCellRadioViewModel model)
        {
            if (ModelState.IsValid)
            {
                SectionCellRadio s = new SectionCellRadio();
                s.name = model.name;
                SectionCell sc = db.SectionCells.SingleOrDefault(i => i.id == sectionCellId);
                if (sc != null)
                {
                    sc.SectionCellRadios.Add(s);
                }
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Update([DataSourceRequest] DataSourceRequest request, SectionCellRadioViewModel model)
        {
            if (ModelState.IsValid)
            {
                SectionCellRadio s = db.SectionCellRadios.SingleOrDefault(i => i.id == model.id);
                s.name = model.name;
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Destroy([DataSourceRequest] DataSourceRequest request, SectionCellRadioViewModel model)
        {
            SectionCellRadio s = db.SectionCellRadios.SingleOrDefault(i => i.id == model.id);
            db.SectionCellRadios.Remove(s);
            db.SaveChanges();

            return Json(ModelState.ToDataSourceResult());
        }
        private IEnumerable<SectionCellRadioViewModel> GetRadios(int sectionCellId)
        {

            return db.SectionCellRadios.Select(seg => new SectionCellRadioViewModel
            {
                id = seg.id,
                name = seg.name
            });
        }
    }
}
