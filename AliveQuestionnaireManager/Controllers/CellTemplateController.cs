﻿﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web.Mvc;

using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using AliveQuestionnaireManager.Models;


namespace AliveQuestionnaireManager.Controllers
{
    public class CellTemplateController : Controller
    {
        // GET: /Section/
        private Entities db = new Entities();
        public ActionResult Index(string id)
        {
            ViewBag.sectionCellId = id;
            return View();
        }
        public ActionResult Read(int sectionId, [DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetCellTemplates(sectionId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Create(int sectionId, [DataSourceRequest] DataSourceRequest request, SectionCellTemplateViewModel model)
        {
            if (ModelState.IsValid)
            {
                CellTemplate s = new CellTemplate();
                s.textLabel = model.textLabel;
                s.cellType = model.cellType;
                Section sc = db.Sections.SingleOrDefault(i => i.id == sectionId);
                if (sc != null)
                {
                    sc.CellTemplates.Add(s);
                }
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Update([DataSourceRequest] DataSourceRequest request, SectionCellTemplateViewModel model)
        {
            if (ModelState.IsValid)
            {
                CellTemplate s = db.CellTemplates.SingleOrDefault(i => i.id == model.id);
                s.textLabel = model.textLabel;
                s.cellType = model.cellType;
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Destroy([DataSourceRequest] DataSourceRequest request, SectionCellTemplateViewModel model)
        {
            CellTemplate s = db.CellTemplates.SingleOrDefault(i => i.id == model.id);
            db.CellTemplates.Remove(s);
            db.SaveChanges();

            return Json(ModelState.ToDataSourceResult());
        }
        private IEnumerable<SectionCellTemplateViewModel> GetCellTemplates(int sectionCellId)
        {

            return db.CellTemplates.Select(seg => new SectionCellTemplateViewModel
            {
                id = seg.id,
                textLabel = seg.textLabel,
                cellType = seg.cellType
            });
        }
    }
}
