﻿﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web.Mvc;

using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using AliveQuestionnaireManager.Models;

namespace AliveQuestionnaireManager.Controllers
{
    public class SectionArrayDefaultController : Controller
    {
        //
        // GET: /Section/
        private Entities db = new Entities();
        public ActionResult Index(string id)
        {
            ViewBag.sectionId = id;
            return View();
        }
        public ActionResult Read(int sectionId, [DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetDefaults(sectionId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Create(int sectionId, [DataSourceRequest] DataSourceRequest request, SectionArrayDefaultViewModel model)
        {
            if (ModelState.IsValid)
            {
                ArrayDefault s = new ArrayDefault();
                s.name = model.name;
                Section sc = db.Sections.SingleOrDefault(i => i.id == sectionId);
                if (sc != null)
                {
                    sc.ArrayDefaults.Add(s);
                }
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Update([DataSourceRequest] DataSourceRequest request, SectionArrayDefaultViewModel model)
        {
            if (ModelState.IsValid)
            {
                ArrayDefault s = db.ArrayDefaults.SingleOrDefault(i => i.id == model.id);
                s.name = model.name;
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Destroy([DataSourceRequest] DataSourceRequest request, SectionArrayDefaultViewModel model)
        {
            ArrayDefault s = db.ArrayDefaults.SingleOrDefault(i => i.id == model.id);
            db.ArrayDefaults.Remove(s);
            db.SaveChanges();

            return Json(ModelState.ToDataSourceResult());
        }
        private IEnumerable<SectionArrayDefaultViewModel> GetDefaults(int sectionId)
        {

            return db.ArrayDefaults.Select(seg => new SectionArrayDefaultViewModel
            {
                id = seg.id,
                name = seg.name
            });
        }
    }
}
