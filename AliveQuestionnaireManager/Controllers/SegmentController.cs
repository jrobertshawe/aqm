﻿﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using AliveQuestionnaireManager.Models;

namespace AliveQuestionnaireManager.Controllers
{
    public class SegmentController : Controller
    {
        //
        // GET: /Section/
        private Entities db = new Entities();
        public ActionResult Index(string id)
        {
            ViewBag.questionnaireId = id;
            return View();
        }
        public ActionResult Read(int questionnaireId, [DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetSegment(questionnaireId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Create(int questionnaireId, [DataSourceRequest] DataSourceRequest request, SegmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                Segment s = new Segment();
                s.name = model.Name;
                Questionnaire q = db.Questionnaires.SingleOrDefault(i => i.id == questionnaireId);
                if (q != null)
                {
                    q.Segments.Add(s);
                }
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Update([DataSourceRequest] DataSourceRequest request, SegmentViewModel model)
        {
            if (ModelState.IsValid)
            {
                Segment s = db.Segments.SingleOrDefault(i => i.id == model.id);
                s.name = model.Name;
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Destroy([DataSourceRequest] DataSourceRequest request, SegmentViewModel model)
        {
            Segment s = db.Segments.SingleOrDefault(i => i.id == model.id);
            db.Segments.Remove(s);
            db.SaveChanges();

            return Json(ModelState.ToDataSourceResult());
        }
        private IEnumerable<SegmentViewModel> GetSegment(int questionnaireId)
        {

            return db.Segments.Select(seg => new SegmentViewModel
            {
                id = seg.id,
                Name = seg.name
            });
        }
    }
}
