﻿﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web.Mvc;

using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using AliveQuestionnaireManager.Models;

namespace AliveQuestionnaireManager.Controllers
{
    public class SectionCellController : Controller
    {
        //
        //
        // GET: /Section/
        private Entities db = new Entities();
        public ActionResult Index(string id)
        {
            ViewBag.sectionId = id;
            return View();
        }
        public ActionResult Read(int sectionId, [DataSourceRequest] DataSourceRequest request)
        {
            return Json(GetSectionCell(sectionId).ToDataSourceResult(request));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Create(int sectionId, [DataSourceRequest] DataSourceRequest request, SectionCellViewModel model)
        {
            if (ModelState.IsValid)
            {
                SectionCell s = new SectionCell();
                s.textLabel = model.textLabel;
                s.image = model.image;
                s.valueName = model.valueName;
                s.keyboardType = model.keyboardTypeObject.name;
                s.onValueChange = model.valueChangeObject.name;
                s.cellType = model.cellTypeObject.name;
                s.valueRequired = model.valueRequired;
                s.sectionId = sectionId;
                Section sec = db.Sections.SingleOrDefault(i => i.id == sectionId);
                if (sec != null)
                {
                    sec.SectionCells.Add(s);
                }
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Destroy([DataSourceRequest] DataSourceRequest request, SectionCellViewModel model)
        {
            SectionCell sc = db.SectionCells.SingleOrDefault(i => i.id == model.id);
            db.SectionCells.Remove(sc);
            db.SaveChanges();

            return Json(ModelState.ToDataSourceResult());
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Editing_Update([DataSourceRequest] DataSourceRequest request, SectionCellViewModel model)
        {
            if (ModelState.IsValid)
            {
                SectionCell s = db.SectionCells.SingleOrDefault(i => i.id == model.id);
                s.textLabel = model.textLabel;
                s.image = model.image;
                s.valueName = model.valueName;
                s.keyboardType = model.keyboardTypeObject.name;
                s.cellType = model.cellTypeObject.name;
                s.valueRequired = model.valueRequired;
                s.onValueChange = model.valueChangeObject.name;
                db.SaveChanges();
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        private List<SectionCellViewModel> GetSectionCell(int sectionId)
        {
              List<SectionCell> list = new List<SectionCell>();
                List<SectionCellViewModel> models = new List<SectionCellViewModel>();
            list  = db.SectionCells.Where(s => s.sectionId == sectionId).ToList();
            foreach (SectionCell sectioncell in list)
              {
                  SectionCellViewModel model = new SectionCellViewModel();
                 model.id = sectioncell.id;
                model.image = sectioncell.image;
                model.valueRequired = (bool)sectioncell.valueRequired;
                model.valueName = sectioncell.valueName;
                model.textLabel = sectioncell.textLabel;
                model.keyboardTypeObject.name = sectioncell.keyboardType;
                model.cellTypeObject.name = sectioncell.cellType;
                model.valueChangeObject.name = sectioncell.onValueChange;
                model.RadioCount = sectioncell.SectionCellRadios.Count();
                models.Add(model);

            }
            return models;
        }
        public JsonResult GetKeyboardType()
        {

            return Json(db.KeyboardTypes.Select(c => new { id = c.id, name = c.name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCellType()
        {

            return Json(db.CellTypes.Select(c => new { id = c.id, name = c.name }), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetOnValueChange()
        {

            return Json(db.ValueChanges.Select(c => new { id = c.id, name = c.name }), JsonRequestBehavior.AllowGet);
        }
    }
}
