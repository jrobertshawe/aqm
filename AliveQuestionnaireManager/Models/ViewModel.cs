﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;

namespace AliveQuestionnaireManager.Models
{
      [System.Serializable]
      public class KeyboardTypeModel
    {
        public int id
        {
            get;
            set;
        }

        public string name
        {
            get;
            set;
        }
      
    }
      [System.Serializable]
      public class OnValueChange
      {
          public int id
          {
              get;
              set;
          }

          public string name
          {
              get;
              set;
          }

      }
      [System.Serializable]
      public class CellTypeModel
      {
          public int id
          {
              get;
              set;
          }

          public string name
          {
              get;
              set;
          }

      }
    public class QuestionnaireViewModel
    {
        [ScaffoldColumn(false)]
        public int id
        {
            get;
            set;
        }

        [Required]
        [DisplayName("Name")]
        public string Name
        {
            get;
            set;
        }
        
    }
    public class SectionCellTemplateViewModel
    {
        [ScaffoldColumn(false)]
        public int id
        {
            get;
            set;
        }

        [Required]
        [DisplayName("Text Label")]
        public string textLabel
        {
            get;
            set;
        }
        [Required]
        [DisplayName("Cell Type")]
        public string cellType
        {
            get;
            set;
        }
    }
    public class SegmentViewModel
    {
        [ScaffoldColumn(false)]
        public int id
        {
            get;
            set;
        }

        [Required]
        [DisplayName("Name")]
        public string Name
        {
            get;
            set;
        }

    }
    public class SectionCellRadioViewModel
    {
        [ScaffoldColumn(false)]
        public int id
        {
            get;
            set;
        }

        [Required]
        [DisplayName("Name")]
        public string name
        {
            get;
            set;
        }

    }
    public class SectionArrayDefaultViewModel
    {
        [ScaffoldColumn(false)]
        public int id
        {
            get;
            set;
        }

        [Required]
        [DisplayName("Name")]
        public string name
        {
            get;
            set;
        }

    }
    public class SectionViewModel
    {
        [ScaffoldColumn(false)]
        public int id
        {
            get;
            set;
        }

        [Required]
        [DisplayName("Name")]
        public string sectionTitle
        {
            get;
            set;
        }
        [Required]
        [DisplayName("Image")]
        public string image
        {
            get;
            set;
        }
        [Required]
        [DisplayName("Value Name")]
        public string valueName
        {
            get;
            set;
        }
        [Required]
        [DisplayName("Array Name")]
        public string arrayName
        {
            get;
            set;
        }
        
        [Required]
        [DisplayName("Hidden")]
        public bool sectionHidden
        {
            get;
            set;
        }
        [Required]
        [DisplayName("Array")]
        public bool sectionArray
        {
            get;
            set;
        }
        [Required]
        [DisplayName("Move to Top")]
        public bool moveToTop
        {
            get;
            set;
        }
        [Required]
        [DisplayName("Recreate Section")]
        public bool recreateSection
        {
            get;
            set;
        }
        [Required]
        [DisplayName("Show Table Footer")]
        public bool showTableFooter
        {
            get;
            set;
        }
        [Required]
        [DisplayName("GPSLocation")]
        public bool valueIsGPSLocation
        {
            get;
            set;
        }
        [Required]
        [DisplayName("Value Automatic")]
        public bool valueAutomatic
        {
            get;
            set;
        }
        public int arrayDefaultCount { get; set; }
        public OnValueChange valueChangeObject { get; set; }
        public SectionViewModel()
        {
            valueChangeObject = new OnValueChange();
        
        }
    }
    public class SectionCellViewModel
    {
        [ScaffoldColumn(false)]
        public int id
        {
            get;
            set;
        }

        [Required]
        [DisplayName("Value Name")]
        public string valueName
        {
            get;
            set;
        }
        [Required]
        [DisplayName("Text Label")]
        public string textLabel
        {
            get;
            set;
        }
       
        [Required]
        [DisplayName("Image")]
        public string image
        {
            get;
            set;
        }
      
        [Required]
        [DisplayName("Value Required")]
        public bool valueRequired
        {
            get;
            set;
        }
        public int RadioCount { get; set; }
        public KeyboardTypeModel keyboardTypeObject { get; set; }
        public CellTypeModel cellTypeObject { get; set; }
        public OnValueChange valueChangeObject { get; set; }
        public SectionCellViewModel()
    {
        keyboardTypeObject = new KeyboardTypeModel();
        cellTypeObject = new CellTypeModel();
        valueChangeObject = new OnValueChange();
        
    }
    }
      
      
    
}